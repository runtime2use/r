#!/bin/bash

export R_PATH="$LANG_PATH/dist"

################################################################################

ronin_require_rlang () {
    echo hello world >/dev/null
}

ronin_include_rlang () {
    motd_text "    -> R      : "$R_PATH
}

################################################################################

ronin_setup_rlang () {
    echo hello world >/dev/null
}

